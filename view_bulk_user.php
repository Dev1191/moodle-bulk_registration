<?php 
require_once(__DIR__.'/../../config.php');
global $CFG, $DB, $PAGE, $USER, $COURSE,$OUTPUT;
require_once('libbulkreg.php');
// Check permissions.
if(!is_siteadmin()){
    redirect($CFG->wwwroot);
}

echo '<br><br><br>';
$addumesg   = optional_param('msg',null,PARAM_TEXT);   
   
$title = get_string('blockuserlist', 'block_bulk_registration');
$PAGE->navbar->add($title);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/blocks/bulk_registration/view_bulk_user.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->css('/blocks/bulk_registration/css/custom.css');
$PAGE->requires->css('/blocks/bulk_registration/css/jquery.dataTables.min.css');


$objbulkuser = new bulk_user_registration();

echo $OUTPUT->header();
?>
<?php 
//id="example" class="display" style="width:100%"
$html = '';
$html .= html_writer::start_tag('table',array('id'=>'bulkusers','class'=>'admintable generaltable display','style'=>'width:100%'));
$html .= html_writer::start_tag('tr',array('class'=>'header c0 centeralign'));
$html .= html_writer::start_tag('th') .'Fullname'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Email'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Designation'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Occupation'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Groups'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Roles'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Managers'. html_writer::end_tag('th');
$html .= html_writer::start_tag('th') .'Created Date'. html_writer::end_tag('th');
// $html .= html_writer::start_tag('th') .'Status'. html_writer::end_tag('th');
$html .= html_writer::end_tag('tr');
$html .= $objbulkuser->getbulkregusers();
$html .= html_writer::end_tag('table');

echo $html;
?>




<script>
$(document).ready(function(){
         $('#bulkusers').DataTable({
                //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                "lengthMenu": [[-1, 200, 150, 80, 30, 10], ["All", 200, 150, 80, 30, 10]]
            });
});
</script>
<?php 




echo $OUTPUT->footer();
?>

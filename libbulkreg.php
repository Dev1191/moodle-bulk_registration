<?php 

require_once(__DIR__.'/../../config.php');
global $CFG, $DB, $PAGE, $USER, $COURSE,$OUTPUT;


class bulk_user_registration{

   /**
     * @param $uploadeduserlists object
    */
    public function insert_upload_user_lists($uploadeduserlists){
        global $CFG, $DB,$USER;
        $bulkuserregistration = '';
   
        $returnurl = new moodle_url('/blocks/bulk_registration/bulk_registration.php');
        if($uploadeduserlists){
           
            foreach($uploadeduserlists as $arrusers){
                if(!$DB->record_exists_sql("SELECT * FROM {user} WHERE email='".$arrusers['emailaddress']."'")){
                    
                     if((self::validname($arrusers['firstname']) == TRUE ) && (self::validname($arrusers['lastname']) == TRUE)){

                        if(self::validbirthdate($arrusers['birthdate']) == TRUE){

                            if(self::validemail($arrusers['emailaddress']) == TRUE){

                                $tblbulkuser = new stdClass();
                                $tblbulkuser->auth = 'ldap';
                                $tblbulkuser->confirmed= 1;
                                $tblbulkuser->mnethostid =1;
                                $tblbulkuser->firstname = $arrusers['firstname'];
                                $tblbulkuser->lastname = $arrusers['lastname'];
                                $tblbulkuser->username =  $arrusers['username'];
                                $tblbulkuser->password = 'not cached';
                                $tblbulkuser->email = $arrusers['emailaddress'];
                                $tblbulkuser->timecreated = time();  
                                $tblbulkuser->city = $arrusers['city'];
                                $userids = $DB->insert_record('user',$tblbulkuser);
                             
                                    if($userids){
                                        $fields = implode(',',array_keys($arrusers));
                                        if($DB->record_exists_sql("SELECT * FROM {user_info_field} WHERE  FIND_IN_SET(shortname,'".$fields."') ")){
                                            $getaddtionalfield = $DB->get_records_sql("SELECT id,shortname FROM {user_info_field} WHERE FIND_IN_SET(shortname,'".$fields."')");
                                            $fieldid = array();
                                             foreach($getaddtionalfield as $addtionalfields){
                                                    $fieldid[$addtionalfields->id] = $arrusers[$addtionalfields->shortname];
                                            }   
                                                foreach($fieldid  as $ids => $data){
        
                                                    $tbladditional = new stdClass();
                                                    $tbladditional->userid = $userids;
                                                    $tbladditional->fieldid =  $ids;
                                                    $tbladditional->data = $data;
                                                    $DB->insert_record('user_info_data',$tbladditional);
                                                    
                                                }
                                        }

                                        $tblusergroup = new stdClass();
                                        $tblusergroup->userid = $userids;
                                        // $tblusergroup->civilstatus = $arrusers['civilstatus'];
                                        // $tblusergroup->gender = $arrusers['gender'];
                                        // $tblusergroup->dob = $arrusers['birthdate'];
                                        // $tblusergroup->designation = $arrusers['designation'];
                                        // $tblusergroup->occupation = $arrusers['occupation'];
                                        $tblusergroup->groupid = $arrusers['usergroup'];
                                        $tblusergroup->role_manager= $arrusers['role-manager'];
                                        $tblusergroup->role_learner = $arrusers['role-learner'];
                                        $tblusergroup->role_educator = $arrusers['role-educator'];
                                        $tblusergroup->role_recruiter = $arrusers['role-recruiter'];
                                        $tblusergroup->role_reporter = $arrusers['role-reporter'];
                                        $tblusergroup->manager1 = $arrusers['manager1'];
                                        $tblusergroup->manager2 = $arrusers['manager2'];
                                        $tblusergroup->manager3 = $arrusers['manager3'];
                                        $tblusergroup->manager4 = $arrusers['manager4'];
                                        $tblusergroup->agencygroup = $arrusers['agencygroup'];
                                        $tblusergroup->createdby = $USER->id;
                                        $tblusergroup->createddate = time();
                                        
                                             $bulkuserregistration = $DB->insert_record('bulk_user_registration',$tblusergroup);
                                             if($bulkuserregistration){
                                                      $this->defineroles($bulkuserregistration);
                                                      $this->changeuserinfo($userids);
                                                    if($this->sentInvitemailtoLDAP($arrusers['emailaddress'],$arrusers['firstname'],$arrusers['lastname']) == TRUE){
                                                       $this->sentInvitemailtoLDAP($arrusers['emailaddress'],$arrusers['firstname'],$arrusers['lastname']);
                                                    }else{
                                                        $this->sentInviteMailLDAP($arrusers['emailaddress'],$arrusers['firstname'],$arrusers['lastname']);   
                                                    }   
                                             }
                                    
                                }

                            }else{
                                return  print_error('csvloaderror', '', $returnurl,'not valid Email Address ');
                            }

                        }else{
                             return  print_error('csvloaderror', '', $returnurl,'not validd Birth Date');
                        }
               

                   }else{
                       return  print_error('csvloaderror', '', $returnurl,'not valid first name and fast name');
                    }
               }
           }
    }
    return  $bulkuserregistration;
}

/**
 * @param $arrfields 
 * **/

 public  function changeuserinfo($userids){
     global $DB;
     $getfieldname = $DB->get_records_sql("SELECT uid.id as uids ,uid.data,uif.shortname FROM {user_info_field} as uif INNER JOIN {user_info_data} as uid ON uid.fieldid=uif.id WHERE uid.userid='".$userids."'");
     foreach($getfieldname as $fieldname){
             if($fieldname->shortname == 'birthdate' ){
                     $tbjdata = new stdClass;
                     $tbjdata->id  = $fieldname->uids;
                     $tbjdata->data = strtotime($fieldname->data);
                       $res = $DB->update_record('user_info_data',$tbjdata);
         } 
     
     }
 }


public function defineroles($regids){
    global $DB;

if($DB->record_exists_sql("SELECT * FROM {bulk_user_registration} WHERE id='".$regids."'")){

   $getroles = $DB->get_record_sql("SELECT userid,role_manager,role_learner,role_educator,role_recruiter,role_reporter FROM {bulk_user_registration} WHERE id='".$regids."'");

    if($getroles->role_manager == '1'){
        self::rolemanage('manager',$getroles->userid);
    }
    if($getroles->role_learner == '1'){
        self::rolemanage('learner',$getroles->userid);
    }
    if($getroles->role_educator == '1'){
        self::rolemanage('educator',$getroles->userid);
    }
    if($getroles->role_recruiter == '1'){
        self::rolemanage('recruiter',$getroles->userid);
    }
    if($getroles->role_reporter == '1'){
        self::rolemanage('reporter',$getroles->userid);
    }
    
    }
 

}

public static function rolemanage($string,$userids){
    global $DB,$USER;

   $checkrolename = $DB->get_record_sql("SELECT id,shortname FROM {role} WHERE shortname='".$string."'");
        if(isset($checkrolename) && !empty($checkrolename)){
            $tblobj = new stdClass;
            $tblobj->roleid = $checkrolename->id;
            $tblobj->contextid = 1;
            $tblobj->userid = $userids;
            $tblobj->timemodified = time();
            $tblobj->modifierid = $USER->id;
            //role_assignments
            $roleassign = $DB->insert_record('role_assignments',$tblobj);
        }

}

    
public function sentInvitemailtoLDAP($email,$fn,$ln){       
    global $CFG,$USER,$DB;

    $fromUser = $USER->email;
    $subject = 'Registration confirmation';

    $messagehtml =   "<p>Hi $fn $ln,</p>
    <p>You have been registered to Allianz Learning Portal. This will give you access to available trainings you may take to further improve your competencies as an Allianz contributor.</p>
    <p>To get started, you may click the button below to redirect you to the Login screen of the Training Portal. You need to use your Active Directory account as authentication and select the correct user group you are currently assigned to. </p>
   
    <p>$CFG->wwwroot/local/login/index.php</p>
        
    <p>You may access the Help / FAQ on the system for guidance on how to use the system or Contact Admin if you encounter any issues.</p>
    <p>This is an auto generated email.</p>

    <p>Thanks!</p>
    ";

    $emailuser = new stdClass();
    $emailuser->email = $email;
    $emailuser->maildisplay = true;
    $emailuser->mailformat = 1; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
    $emailuser->id = 1;
    $emailuser->firstnamephonetic = false;
    $emailuser->lastnamephonetic = false;
    $emailuser->middlename = false;
    $emailuser->alternatename = false;
     $mail = email_to_user($emailuser,$fromUser, $subject, $message = '', $messagehtml);
    if($mail){
        return TRUE;
    }else{
        return FALSE;
    }

}



    /**
     * send email to bulk uploaded users
     * @param $email string
     * @param $fn string
     * @param $ln string
     * **/
    public  function sentInviteMailLDAP($mail,$fn,$ln){
        global $CFG,$USER,$DB;
        $massage =   "Hi $fn $ln,
        
        You have been registered to Allianz Learning Portal. This will give you access to available trainings you may take to further improve your competencies as an Allianz contributor.
        To get started, you may click the button below to redirect you to the Login screen of the Training Portal. You need to use your Active Directory account as authentication and select the correct user group you are currently assigned to. 
       
        $CFG->wwwroot/local/login/index.php

        You may access the Help / FAQ on the system for guidance on how to use the system or Contact Admin if you encounter any issues.
        This is an auto generated email.

        Thanks!
        ";
          $to      = $mail;
          $subject = 'Registration confirmation';
          $message =$massage ;
          $headers = 'From: Allianz' . "\r\n" .
          'Reply-To: lsd' . "\r\n" .
          'X-Mailer: PHP/' . phpversion();
          $m = mail($to, $subject, $message, $headers);        
        }


    
    /**
     * convert the csv data content to assocative arrays
     * @param $type string
     * @param $iid integer
     * **/
    public function csv_to_array($type,$iid)
    {
        global $CFG,$USER;
        $filename = $CFG->tempdir.'/csvimport/'.$type.'/'.$USER->id.'/'.$iid;
        
        $delimiter=',';
        
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
    
        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * @param $uploadeduserlist array object
     * @return array uploaded user list
     * ***/
    public  function validate_all_csv_rows($uploadeduserlist){
        $noofcolumns = '';
        $noofrows = '';
        $arruploaduserlist = array();
        foreach($uploadeduserlist as $uploaduserlist){
            foreach($uploaduserlist as $key => $definitevalue){
                $arrkey = explode(',',strtolower($key));
                $arrusers = explode(',',$definitevalue);
                $noofcolumns = count($arrkey);
                $noofrows = count($arrusers);
                if($noofcolumns === $noofrows){
                    array_push($arruploaduserlist,array_combine($arrkey,$arrusers));
                }else{
                    $arruploaduserlist = 'Error must be have columns not equal to rows';
                }
                
            }
        }

        return $arruploaduserlist;

    }

        /**
     * @param dateofbirth validate email address
     * @return TRUE/FALSE 
     * @date will DD-MM-YYYY
     * **/
    public static function validbirthdate($daterows){
        $date = explode('-',$daterows);
                     // month     day      year 
        if (checkdate($date[1],$date[0],$date[2])){
                return TRUE;
                } else {
                    return FALSE;
                }

     }

    /**
     * @param email validate email address
     * @return TRUE/FALSE 
     * **/
    public static function validemail($datarows){
        if(preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i",$datarows)){
            return TRUE;
         }else{
            return FALSE;

         }

    }

        /**
     * validate the email address
     *  @param $datarow email
     * **/
    public static function validname($datarows){
        if (!preg_match("/[^A-Za-z\s-]/i", trim($datarows))) {
                return TRUE;
            }else{
            return FALSE;

            }
    }



    public function getbulkregusers(){
        global $CFG,$USER,$DB;
        $html ='';
        $getladapusers = $DB->get_records_sql("SELECT u.firstname,u.lastname,u.email,bur.* FROM {bulk_user_registration} as bur
        INNER JOIN {user} as u ON u.id=bur.userid  ORDER BY bur.id DESC");
       
        foreach($getladapusers as $userlist){
            $html .= html_writer::start_tag('tr',array('class'=>'header c0 centeralign'));
            $html .= html_writer::start_tag('td').'<a href="'.$CFG->wwwroot.'/user/profile.php?id='.$userlist->userid.'" target="_blank">'
            .' '.$userlist->firstname.' '.$userlist->lastname.'</a>'.html_writer::end_tag('td');
            $html .= html_writer::start_tag('td').$userlist->email. html_writer::end_tag('td');


            $adddata =  self::getaddtionailuserdetails($userlist->userid);
            if(empty($userlist->designation)){
                $html .= html_writer::start_tag('td'). $adddata[0]. html_writer::end_tag('td');
            }else{
                $html .= html_writer::start_tag('td').$userlist->designation. html_writer::end_tag('td');
            }
            if(empty($userlist->occupation)){
                $html .= html_writer::start_tag('td'). $adddata[1]. html_writer::end_tag('td');
            }else{
                $html .= html_writer::start_tag('td').$userlist->occupation. html_writer::end_tag('td');
            }

            // $html .= html_writer::start_tag('td').$userlist->designation. html_writer::end_tag('td');
            // $html .= html_writer::start_tag('td').$userlist->occupation. html_writer::end_tag('td');


           $getgroupname = $DB->get_record_sql("SELECT name FROM {groups} WHERE id='".$userlist->groupid."'");
            $html .= html_writer::start_tag('td').'<span class="label label-primary">'.$getgroupname->name.'</span>'. html_writer::end_tag('td');
            $html .= html_writer::start_tag('td').'<span class="label label-primary">'.self::getrolename($userlist->role_manager,'manager').'</span>
            <span class="label label-primary">'.self::getrolename($userlist->role_learner,'learner').'</span>
            <span class="label label-primary">'.self::getrolename($userlist->role_educator,'educator').'</span>
            <span class="label label-primary">'.self::getrolename($userlist->role_recruiter,'recruiter').'</span>
            <span class="label label-primary">'.self::getrolename($userlist->role_reporter,'reporter').'</span>'
            . html_writer::end_tag('td');
            $html .= html_writer::start_tag('td').'<span class="label label-primary">'.$userlist->manager1.'</span>
            <span class="label label-primary">'.$userlist->manager2.'</span>
            <span class="label label-primary">'.$userlist->manager3.'</span>
            <span class="label label-primary">'.$userlist->manager4.'</span>'
            . html_writer::end_tag('td');
            $html .= (date('d/m/Y',$userlist->createddate) != '01/01/1970') ? html_writer::start_tag('td').date('M d Y',$userlist->createddate). html_writer::end_tag('td') :
            html_writer::start_tag('td').'null'. html_writer::end_tag('td');

            $html .= html_writer::end_tag('tr');
        }

            return $html;
    }



public static function getaddtionailuserdetails($userids){
    globaL $DB;
  $arrad = array();
$getfieldname = $DB->get_records_sql("SELECT uid.data,uif.shortname FROM {user_info_field} as uif INNER JOIN {user_info_data} as uid ON uid.fieldid=uif.id WHERE uid.userid='".$userids."'");
    foreach($getfieldname as $fieldname){

        if($fieldname->shortname == 'designation' ){
            array_push($arrad,$fieldname->data);
         }

            if($fieldname->shortname == 'occupation' ){
                    array_push($arrad,$fieldname->data);
            }
   
    }
      return  $arrad ;
}




    /**
     * find the role id base on the $string value
     * @param $roleid integer
     * @param $string text 
     * @return string role name
     * **/
    public static function getrolename($roleid,$string){
        global $DB,$CFG;
        switch ($string) {
            case 'manager' : 
                    if(!empty($roleid) == '1'){
                        $getname = $DB->get_record_sql("SELECT shortname FROM {role} WHERE TRIM(shortname)='".$string."'");
                        return $getname->shortname;
                    }else{
                        return FALSE;
                    }
                    break;
            case 'learner' : 
                    if(!empty($roleid) == '1'){
                        $getname = $DB->get_record_sql("SELECT shortname FROM {role} WHERE TRIM(shortname)='".$string."'");
                        return $getname->shortname;
                    }else{
                        return FALSE;
                    }
                    break;
           case 'educator' : 
                    if(!empty($roleid) == '1'){
                        $getname = $DB->get_record_sql("SELECT shortname FROM {role} WHERE TRIM(shortname)='".$string."'");
                        return $getname->shortname;
                    }else{
                        return FALSE;
                    }
                    break;
            case 'recruiter' : 
                    if(!empty($roleid) == '1'){
                        $getname = $DB->get_record_sql("SELECT shortname FROM {role} WHERE TRIM(shortname)='".$string."'");
                        return $getname->shortname;
                    }else{
                        return FALSE;
                    }
                    break;
           case 'reporter' : 
                    if(!empty($roleid) == '1'){
                        $getname = $DB->get_record_sql("SELECT shortname FROM {role} WHERE TRIM(shortname)='".$string."'");
                        return $getname->shortname;
                    }else{
                        return FALSE;
                    }
                    break;
            default : 
                    return 'not found';
        }


    }



        /**
     * redirect url with message
     * @param $msg integer
     * @return text alert message after sucessfulyy uploaded LDAP users
     **/
    public function redirectsuccessmsg($msg){
        global $CFG;
        $html = '';
        if($msg == '1'){
            $html .= html_writer::start_tag('div',array('class'=>'alert alert-success'));
            $html .= html_writer::start_tag('i', array('class'=>'icon fa fa-check','aria-hidden'=>'true')). html_writer::end_tag('i');
            $html .= get_string('bulkinviteuser','block_bulk_registration');
            $html .= html_writer::start_tag('a',array('href'=>'#','class'=>'close','data-dismiss'=>'alert','aria-label'=>'close')).'&times'.html_writer::end_tag('a');
            $html .= html_writer::end_tag('div');

        }
        return $html;

    }


    

}
<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add event handlers for the quiz
 *
 * @package    mod_enrollment
 * @category   event
 * @copyright  2011 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

$observers  = array(
     array(
        'eventname' => '\core\event\role_unassigned',
        'includefile' =>'blocks/bulk_registration/roleunassignlib.php',
        'callback' => 'user_role_unassigned',
           'internal' => false
     ),
     array(
      'eventname' => '\core\event\role_assigned',
      'includefile' =>'blocks/bulk_registration/roleunassignlib.php',
      'callback' => 'user_role_assigned',
         'internal' => false
   ),
    array(
      'eventname' => '\core\event\user_deleted',
      'includefile' =>'blocks/bulk_registration/userdeletelib.php',
      'callback' => 'set_user_deleted',
         'internal' => false
   ),


);


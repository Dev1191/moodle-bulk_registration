<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * bulk_registration block caps.
 *
 * @package    block_bulk_registration
 * @copyright   Dave Raj <devraj@ldsengineers.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class block_bulk_registration extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_bulk_registration');
    }

    function get_content() {
        global $CFG, $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }
        $context = get_context_instance(CONTEXT_SYSTEM);
        $roles = get_user_roles($context, $USER->id, false);
        $role = key($roles);
        $currentsystemroleid = $roles[$role]->roleid;
        $arrrole = array();
        foreach ($roles as $r) {
            array_push($arrrole, $r->roleid);
        }


        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

        $viewuserlists = $CFG->wwwroot.'/blocks/bulk_registration/bulk_registration.php';
        $viewuserlist1 = $CFG->wwwroot.'/blocks/bulk_registration/view_bulk_user.php';
        $viewuserlist2 = $CFG->wwwroot.'/local/login/customauthnticate.php';
        
        if(is_siteadmin() ||  in_array('1', $arrrole) ){
            $this->content->footer  .= html_writer::tag('a', get_string('viewuserlists', 'block_bulk_registration'), array('href' => $viewuserlists,'target'=>'_blank')).'<br>';
            $this->content->footer  .= html_writer::tag('a', get_string('viewbulkuser', 'block_bulk_registration'), array('href' => $viewuserlist1,'target'=>'_blank')).'<br>';    
            $this->content->footer  .= html_writer::tag('a', get_string('ldapauthenticate', 'block_bulk_registration'), array('href' => $viewuserlist2,'target'=>'_blank'));    
       
       
        }

        return $this->content;
    }

    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => true,
                     'site' => true,
                     'site-index' => true,
                     'course-view' => true, 
                     'course-view-social' => false,
                     'mod' => true, 
                     'mod-quiz' => false);
    }

    public function instance_allow_multiple() {
          return true;
    }

    function has_config() {return true;}

    public function cron() {
            mtrace( "Hey, my cron script is running" );
             
                 // do something
                  
                      return true;
    }
}

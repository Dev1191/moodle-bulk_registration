<?php

defined('MOODLE_INTERNAL') || die;
require_once($CFG->libdir.'/formslib.php');



class bulk_registration_form extends moodleform {

       /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform    = $this->_form;

        $mform->addElement('header', 'addusergeneral1', get_string('blockuserbulkuploadsettings', 'block_bulk_registration'));
        $mform->setExpanded('addusergeneral1');
   
        
        $mform->addElement('html','<div class="pull-right"><a href="'.$CFG->wwwroot.'/blocks/bulk_registration/template.php"><i class="fa fa-file" aria-hidden="true"></i> Download file template</a></div>');
        $mform->addElement('filepicker', 'userfile',get_string('attachment', 'block_bulk_registration'), null, array('maxbytes' => $maxbytes, 'accepted_types' => '*'));
        $mform->addRule('userfile', get_string('fileupload', 'block_bulk_registration'), 'required');

        $mform->addElement('submit', 'adduser', get_string('btnadduser', 'block_bulk_registration'), array('class' => 'form-submit'));
    }

  


}

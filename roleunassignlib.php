<?php


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/enrollib.php');
require_once($CFG->libdir . '/eventslib.php');
require_once('libbulkreg.php');

$objbulk = new bulk_user_registration();


defined('MOODLE_INTERNAL') || die();

require_once (dirname(__FILE__).'/../../config.php');



/**
 * @param array $event;
 * @return bool updated auto role assign
 * **/
function user_role_unassigned(\core\event\role_unassigned $event) {
        global $DB, $CFG, $USER;
    

     $getRoleDetail = $DB->get_record_sql("SELECT shortname FROM {role} WHERE id='".$event->objectid."'");
     $rolename = $getRoleDetail->shortname;
     $getuserbulk = $DB->get_record_sql("SELECT id FROM {bulk_user_registration} WHERE userid='".$event->relateduserid."'");
                        
                        $unassignrole = new stdClass();
                        $unassignrole->id = $getuserbulk->id;
                        
                        if(strcmp($rolename,'manager') == 0 ){             
                                $unassignrole->role_manager = 0;
                        }else if(strcmp($rolename,'learner') == 0){
                
                                $unassignrole->role_learner = 0;             
                        }else if(strcmp($rolename,'educator') == 0)  {
                                $unassignrole->role_educator = 0;
                        
                        }else if(strcmp($rolename,'recruiter') == 0){
                           
                                $unassignrole->role_recruiter = 0;
                        
                        }else if(strcmp($rolename,'reporter')== 0) {
                                $unassignrole->role_reporter = 0;
                        }

                        $DB->update_record('bulk_user_registration',$unassignrole);
            return TRUE;
}



/**
 * @param array $event;
 * @return bool updated auto role assign
 * **/
function user_role_assigned(\core\event\role_assigned $event){

        global $DB, $CFG, $USER;

        $getRoleDetail = $DB->get_record_sql("SELECT shortname FROM {role} WHERE id='".$event->objectid."'");
        $rolename = $getRoleDetail->shortname;
        $getuserbulk = $DB->get_record_sql("SELECT id FROM {bulk_user_registration} WHERE userid='".$event->relateduserid."'");

                        
        $assignrole = new stdClass();
        $assignrole->id = $getuserbulk->id;

        if(strcmp($rolename,'manager') == 0 ){             
                $assignrole->role_manager = 1;
        }else if(strcmp($rolename,'learner') == 0){

                $assignrole->role_learner = 1;             
        }else if(strcmp($rolename,'educator') == 0)  {
                $assignrole->role_educator = 1;
        
        }else if(strcmp($rolename,'recruiter') == 0){
           
                $assignrole->role_recruiter = 1;
        
        }else if(strcmp($rolename,'reporter')== 0) {
                $assignrole->role_reporter = 1;
        }

       $DB->update_record('bulk_user_registration',$assignrole);

return  TRUE;
}



<?php 
require_once(__DIR__.'/../../config.php');
global $CFG, $DB, $PAGE, $USER,$OUTPUT;
require_once('bulk_registration_form.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once('libbulkreg.php');
$success   = optional_param('success',null,PARAM_INT);   
require_login();
$title = get_string('blockstring', 'block_bulk_registration');
$PAGE->navbar->add($title);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/blocks/bulk_registration/bulk_registration.php');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$returnurl = new moodle_url('/blocks/bulk_registration/bulk_registration.php');

$objuserbulk = new bulk_user_registration();

 $mform = new bulk_registration_form();


echo $OUTPUT->header();

    if(empty($success)){
        if ($fromform = $mform->is_cancelled()) {
            redirect($returnurl);
        }else if ($fromform = $mform->get_data()) {
                
            $iid = csv_import_reader::get_new_iid('uploaduser');
            $cir = new csv_import_reader($iid, 'uploaduser');
        
            $content = $mform->get_file_content('userfile');
        
            $readcount = $cir->load_csv_content($content,'utf-8','semicolon');
        
            $csvloaderror = $cir->get_error();
            unset($content);
        
            if (!is_null($csvloaderror)) {
                print_error('csvloaderror', '', $returnurl, $csvloaderror);
            }
            
            $uploadeduserlist = $objuserbulk->csv_to_array('uploaduser',$iid);
        $validuserlist = $objuserbulk->validate_all_csv_rows($uploadeduserlist);    
        $res = $objuserbulk->insert_upload_user_lists($validuserlist);
            if($res){
                    
                    $returnurlmsg =  new moodle_url('/blocks/bulk_registration/bulk_registration.php?success=1');
                    redirect($returnurlmsg);
                }
            
    
        } else {
        
            $mform->display();
        }
    }else{

        $mform->display();
        echo $objuserbulk->redirectsuccessmsg($success);
    }
        
echo $OUTPUT->footer();